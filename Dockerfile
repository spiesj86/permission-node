FROM debian:stable-slim

# Geth Binary URL
ARG geth_binary_url=https://gethstore.blob.core.windows.net/builds/geth-linux-amd64-1.9.9-01744997.tar.gz
# Which Blockchain Network
ARG blockchain_network=mainnet
# ASK Bucket
ARG genesis_data_bucket=ask-blockchain

RUN apt-get update
RUN apt-get install -y \
    apt-utils \
    curl \
    tar \
    jq

RUN curl -L ${geth_binary_url} | tar xvz --strip-components=1 -C /usr/local/bin

RUN mkdir -p /opt/blockchain/node \
    mkdir /opt/blockchain/config \
    mkdir /opt/blockchain/data

COPY config.json /opt/blockchain/config/config.json
COPY run-geth.sh /opt/blockchain/run-geth.sh

RUN chmod a+x /opt/blockchain/run-geth.sh

RUN curl https://s3.amazonaws.com/${genesis_data_bucket}/${blockchain_network}/genesis.json -o /opt/blockchain/config/genesis.json
RUN geth --datadir /opt/blockchain/data/ init /opt/blockchain/config/genesis.json

EXPOSE 30303
CMD /opt/blockchain/run-geth.sh
